package trivadis.vehicle;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.util.Base64;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Vehicle {
	private static final Logger log = LoggerFactory.getLogger(Vehicle.class.getName());

	private static final CustomObjectMapper om = new CustomObjectMapper();
	private static int timeToWait = 5;
	private static double timeMultiplicator = 60;
	public static final int MAX_FUEL = 300;
	public static final int LOW_FUEL = 30;
	public static final int AVERAGE_SPEED = 55;
	public static final int SLOW_SPEED = 30;
	public static final int CLOSE_TO_DESTINATION = 15;
	public static final int ARRIVED_DISTANCE = 5;
	// average consume L / km
	public static final double AVARAGE_FUEL_CONSUME = 0.33;
	// KM per decimal degree
	public static final double oneSecondLat = 111.32;
	// KM per decimal degree
	public static final double oneSecondLon = 90;
	// Glattbrugg, Bern, Stuttgart,Innsbruck,Milano
	public static double[][] locations = { { 47.425210, 8.561343 }, { 47.017423, 7.486764 }, { 48.705954, 9.123819 },
			{ 47.264898, 11.353426 }, { 45.743215, 9.067090 } };

	public static void main(String[] args) throws IOException {
		Random r = new Random();
		if (args.length < 1) {
			System.out.println(
					"Provide  agruments: <vin number> <delay, default 5s> <time multiplicator, default 60s.0>");
			System.exit(0);
		}
		if (args.length > 1) {
			timeToWait = Integer.valueOf(args[1]);
		}
		if (args.length > 2) {
			timeMultiplicator = Double.valueOf(args[2]);
		}
		// Initialize tracking record with vin and current time
		TrackingRecord record = new TrackingRecord(args[0]);
		int startIndex = r.nextInt(locations.length);
		int destinationIndex = startIndex;// r.nextInt(locations.length);
		log.info("Start index {} , DestIndex {}", startIndex, destinationIndex);
		record.setLat(locations[startIndex][0]);
		record.setLon(locations[startIndex][1]);
		record.setDistance(Math.round(Math.random() * 1000000));
		record.setRemainingFuel(MAX_FUEL - r.nextInt(100));
		record.setWeight(r.nextInt(8) * 5000 + 3000);
		record.setWaterTemp(Math.random() * 15 + 80);
		record.setOilTemp(Math.random() * 20 + 90);
		record.setMotorRunningTime(Math.round(Math.random() * 1000 * 3600 * 100));
		record.setPlateNumber("ZH " + Math.round(Math.random() * 1000) + Math.round(Math.random() * 1000));

		send(record);
		// Save tracking location to file, you cat remove it
		/*
		 * FileOutputStream fos = new FileOutputStream("Points.csv",false);
		 * fos.write("lat,lng\r\n".getBytes("UTF-8"));
		 */
		while (true) {
			waitMs(timeToWait * 1000);
			int currentSpeed = AVERAGE_SPEED + r.nextInt(40);
			long timeSpend = Math.round(timeToWait * 1000 * timeMultiplicator);
			// copy fixed fields
			TrackingRecord next = new TrackingRecord(record.getVin());
			next.setPlateNumber(record.getPlateNumber());
			Date eventTime = new Date(record.getEventTime().getTime() + timeSpend);
			next.setEventTime(eventTime);
			next.setWeight(record.getWeight());
			next.setOilTemp(record.getOilTemp());
			next.setWaterTemp(record.getWaterTemp());
			// Check if we reached destination
			double currentDistanceToTarget = getDistance(record.getLat(), record.getLon(),
					locations[destinationIndex][0], locations[destinationIndex][1]);
			log.info("current distance to target {} km, destination {}", currentDistanceToTarget, destinationIndex);
			if (currentDistanceToTarget < ARRIVED_DISTANCE) {
				log.info("Arrived to destination");
				next.setLat(locations[destinationIndex][0]);
				next.setLon(locations[destinationIndex][1]);
				next.setRemainingFuel(MAX_FUEL - r.nextInt(100));
				next.setWeight(r.nextInt(8) * 5000 + 3000);
				startIndex = destinationIndex;
				destinationIndex = r.nextInt(locations.length);
				currentSpeed = 0;
			} else if (currentDistanceToTarget < CLOSE_TO_DESTINATION) {
				// we are close to destination
				log.info("close to destination");
				currentSpeed = SLOW_SPEED;
			}
			if (record.getRemainingFuel() < LOW_FUEL) {
				log.info("low fuel");
				currentSpeed = 0;
				record.setRemainingFuel(MAX_FUEL);
			}
			if (currentSpeed > 0) {
				next.setMotorRunningTime(record.getMotorRunningTime() + timeSpend);
			} else {
				next.setMotorRunningTime(record.getMotorRunningTime());
			}
			double drivedKm = currentSpeed * timeSpend / 1000 / 3600;
			log.info("currect speed {}, km drived {}", currentSpeed, drivedKm);
			// Calculate new coordinates
			double lattitudeDiff = locations[destinationIndex][0] - locations[startIndex][0];
			double lognitudeDiff = locations[destinationIndex][1] - locations[startIndex][1];
			double lattitudeDistance = lattitudeDiff * oneSecondLat;
			double longitudeDistance = lognitudeDiff * oneSecondLon;
			double totalDistance = Math
					.sqrt(lattitudeDistance * lattitudeDistance + longitudeDistance * longitudeDistance);
			if (totalDistance > 0) {
				double lattitudeDiffMult = lattitudeDistance / totalDistance;
				double longitudeDiffMult = longitudeDistance / totalDistance;
				double newLattitude = record.getLat() + drivedKm * lattitudeDiffMult / oneSecondLat;
				double newLongitude = record.getLon() + drivedKm * longitudeDiffMult / oneSecondLon;
				next.setLat(newLattitude);
				next.setLon(newLongitude);
			} else {
				next.setLat(record.getLat());
				next.setLon(record.getLon());
			}
			next.setRemainingFuel(record.getRemainingFuel() - drivedKm * AVARAGE_FUEL_CONSUME);
			next.setDistance(Math.round(record.getDistance() + drivedKm));
			send(next);
			record = next;
			/*
			 * String str = next.getLat() + "," + next.getLon() + "," +
			 * currentDistanceToTarget + "\r\n"; fos.write(str.getBytes("UTF-8"));
			 * fos.flush();
			 */
		}

	}

	public static double getDistance(final double lat1, final double lon1, final double lat2, final double lon2) {
		double lattitudeDiff = lat1 - lat2;
		double lognitudeDiff = lon1 - lon2;
		double lattitudeDistnce = lattitudeDiff * oneSecondLat;
		double longitudeDistance = lognitudeDiff * oneSecondLon;
		return Math.sqrt(lattitudeDistnce * lattitudeDistnce + longitudeDistance * longitudeDistance);
	}

	private static void waitMs(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	private static void send(TrackingRecord record) {
		try {
			log.info("sending {}", om.writeValueAsString(record));

			URL url = new URL("http://localhost:9090/vehicles/insert");
			HttpURLConnection http = (HttpURLConnection) url.openConnection();

			String authStr = "PlanetExpress" + ":" + "secret";
			byte[] bytesEncoded = Base64.getEncoder().encode(authStr.getBytes());
			String authEncoded = new String(bytesEncoded);
			http.setRequestProperty("Authorization", "Basic " + authEncoded);

			http.setDoOutput(true);
			http.setRequestMethod("POST");
			OutputStream out = http.getOutputStream();

			om.writeValue(out, record);
			out.flush();
			out.close();

			log.info("response code {}", http.getResponseCode());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
